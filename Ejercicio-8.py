aspirantes = int(input("¿Cuántos aspirantes entrevistará?\n"))
sumaedad = 0
contratado = 0
for i in range(aspirantes):
    año = int(input("Introduzca el año en que nacieron los aspirantes: "))
    edad = 2020 - año
    print("Edad del aspirante n°",i+1,": ",edad)
    resp = str(input("¿Contrata a este aspirante?(s/n)): "))
    if resp == "s" or resp == "S":
        sumaedad = sumaedad + edad
        contratado = contratado + 1
media = sumaedad / contratado
print("La edad promedio de las personas contratadas: ",media)